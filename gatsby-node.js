const urlSlug = require('url-slug');

exports.createPages = async ({ actions, graphql, reporter }) => {
  const data = await graphql(`
    query {
      allStrapiPropiedades {
        propiedades: nodes {
          id,
          nombre
        }
      }
      allStrapiPaginas {
        paginas: nodes {
          id
          nombre: Nombre
        }
      }
    }
  `);

  //
  if (data.errors) {
    reporter.panic("No hubo resultados", data.errors);
  }

  const propiedades = data.data.allStrapiPropiedades.propiedades;

  const paginas = data.data.allStrapiPaginas.paginas;

  // Crea los templates para las propiedades
  propiedades.forEach(propiedad => {
    //urlSlug(propiedad.nombre)
    actions.createPage({
      path: urlSlug(propiedad.nombre),
      component: require.resolve('./src/components/Propiedad.jsx'),
      context: {
        id: propiedad.id
      }
    })
  });

  // Crea los templates para las paginas
  paginas.forEach(pagina => {
    //urlSlug(propiedad.nombre)
    actions.createPage({
      path: urlSlug(pagina.nombre),
      component: require.resolve('./src/components/Paginas.jsx'),
      context: {
        id: pagina.id,
      }
    })
  });

}