import { graphql, useStaticQuery } from 'gatsby';

const usePropiedades = () => {

  const { allStrapiPropiedades: { nodes } } = useStaticQuery(
    graphql`
      query {
        allStrapiPropiedades {
          nodes {
            id
            nombre
            descripcion
            wc,
            precio,
            estacionamiento,
            habitaciones,
            categoria {
              id
              Nombre
            }
            agente {
              id
              Nombre
              Telefono
              Correo
            }
            imagen {
              sharp: childImageSharp {
                fluid ( quality: 90, maxWidth: 600, maxHeight: 400 ) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    `
  );

  

  return nodes.map(propiedad => {
    const { agente, categoria, descripcion, estacionamiento, habitaciones, id, imagen: { sharp: { fluid } }, nombre, precio, wc } = propiedad;
    return {
      agente,
      categoria,
      descripcion,
      estacionamiento,
      habitaciones,
      id,
      imagen: fluid,
      nombre,
      precio,
      wc
    }
  });
};

export default usePropiedades;