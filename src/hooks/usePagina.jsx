import { graphql, useStaticQuery } from 'gatsby';

const usePagina = () => {

  const data = useStaticQuery(
    graphql`
      query {
        allStrapiPaginas(filter: { Nombre: { eq: "Inicio" } }) {
          paginas: nodes {
            id
            Nombre,
            Contenido
            Imagen {
              sharp: childImageSharp {
                fluid( maxWidth: 1200 duotone: {
                  highlight: "#222222", shadow: "#192550", opacity: 30
                }) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    `
  );

  const { Nombre, Contenido, Imagen } = data.allStrapiPaginas.paginas[0];

  return {
    nombre: Nombre,
    contenido: Contenido,
    imagen: Imagen.sharp.fluid
  };
};

export default usePagina;