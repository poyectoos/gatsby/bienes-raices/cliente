import React, { useState } from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import styled from '@emotion/styled';

/*
==================================================
          Emotion
==================================================
*/
const Formulario = styled.form`
  max-width: 1200px;
  width: 95%;
  display: flex;
  margin: 2rem auto 0 auto;
  border: 1px solid #E1E1E1;
`;
const InputSelect = styled.select`
  flex: 1;
  padding: 1rem;
  appearance: none;
  border: none;
  font-family: 'Lato', sans-serif;
`;

const useFiltro = () => {

  const [ categoria, setCategoria ] = useState('');

  const { allStrapiCategorias: { categorias } } = useStaticQuery(
    graphql`
      query {
        allStrapiCategorias {
          categorias: nodes {
            id
            Nombre
          }
        }
      }
    `
  );

  const handleChange = e => {
    setCategoria(e.target.value);
  }
  
  const Select = () => (
    <Formulario>
      <InputSelect
        name="categoria"
        id="categoria"
        value={ categoria }
        onChange={ handleChange }
      >
        <option value="">Selecciona una categoria</option>
        {
          categorias.map(
            opcion => (
              <option
                key={ opcion.id }
                value={ opcion.Nombre }
              >
                { opcion.Nombre }
              </option>
            )
          )
        }
      </InputSelect>
    </Formulario>
  );

  return {
    categoria,
    
    Select
  }
};

export default useFiltro;