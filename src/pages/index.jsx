import React from 'react';
import styled from '@emotion/styled';
import BackgroundImage from 'gatsby-background-image';

import usePagina from '../hooks/usePagina';
/*
==================================================
          Components
==================================================
*/
import Layout from '../components/Layout';
import Encuentra from '../components/Encuentra';
import Propiedades from '../components/Propiedades';

import Hero from '../css/hero.module.css';
/*
==================================================
          Emotion
==================================================
*/
const Container = styled.div`
  max-width: 800px;
  margin: 0 auto;
`;
const Content = styled.p`
  text-align: center;
`;
const Banner = styled(BackgroundImage)`
  height: 350px;
`;

const Index = () => {

  const pagina = usePagina();

  const { nombre, contenido, imagen } = pagina;

  return (
    <Layout>
      <main>
        <Banner
          tag="section"
          fluid={ imagen }
          fadeIn="soft"
        >
          <div className={ Hero.imagen }>
            <h1 className={ Hero.titulo }>Casas y departamentos exclusivos</h1>
          </div>
        </Banner>
        <Container>
          <h1>{ nombre }</h1>
          <Content>{ contenido }</Content>

        </Container>
      </main>
      <Encuentra />
      <Propiedades />
    </Layout>
  );
};

export default Index;