import React from 'react';

/*
==================================================
          Component
==================================================
*/
import Layout from '../components/Layout';

const Component404 = () => {
  return (
    <Layout>
      <h1>404</h1>
      <h2>Página no encontrada</h2>
    </Layout>
  );
};

export default Component404;