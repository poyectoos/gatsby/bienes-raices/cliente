import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import styled from '@emotion/styled';

/*
==================================================
          Emotion
==================================================
*/
const Listado = styled.ul`
  display: flex;
  justify-content: space-between;
  flex: 1;
  max-width: 500px;
  margin: 3rem auto 0 auto;
`;
const Icon = styled.li`
  display: flex;
`;
const Img = styled.img`
  margin-right: 1rem;
`;

const Iconos = ({ estacionamiento, habitaciones, wc }) => {

  const { allFile: { iconos } } = useStaticQuery(
    graphql`
      query {
        allFile ( filter: { relativeDirectory: { eq: "iconos" } } ) {
          iconos: edges {
            node {
              id,
              publicURL
            }
          }
        }
      }
    `
  );

  return (
    <Listado>
      <Icon>
        <Img src={ iconos[2].node.publicURL } alt="wc"/>
        <p>{ wc }</p>
      </Icon>
      <Icon>
        <Img src={ iconos[1].node.publicURL } alt="estacionamiento"/>
        <p>{ estacionamiento }</p>
      </Icon>
      <Icon>
        <Img src={ iconos[0].node.publicURL } alt="habitaciones"/>
        <p>{ habitaciones }</p>
      </Icon>
    </Listado>
  );
};

export default Iconos;