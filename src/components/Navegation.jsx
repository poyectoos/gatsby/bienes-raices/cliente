import React from 'react';
import { Link } from 'gatsby';
import styled from '@emotion/styled';

/*
==================================================
          Emotion
==================================================
*/
const Nav = styled.nav`
  display: flex;
  flex-direction: column;
  padding-bottom: 1rem;

  
  @media (min-width: 768px) {
    flex-direction: row;
    padding-bottom: 0;
  }
`;
const A = styled(Link)`
  color: #FFFFFF;
  margin-right: 1rem;
  text-decoration: none;
  font-weight: 700;
  font-family: 'PT Sans', sans-serif;

  &:last-of-type {
    margin-right: 0;
  }

  &.active {
    color: #0dcaf0;
  }
`;

const Navegation = () => {
  return (
    <Nav>
      <A
        to={'/'}
        activeClassName="active"
      >
        Inicio
      </A>
      <A
        to={'/nosotros'}
        activeClassName="active"
      >
        Nosotros
      </A>
      <A
        to={'/propiedades'}
        activeClassName="active"
      >
        Propiedades
      </A>
    </Nav>
  );
};

export default Navegation;