import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
import { graphql } from 'gatsby';

/*
==================================================
          Components
==================================================
*/
import Layout from './Layout';
import Iconos from '../components/Iconos';

/*
==================================================
          Emotion
==================================================
*/
const Contenido = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  width: 95%;

  @media(min-width: 768px) {
    display: grid;
    grid-template-columns: 2fr 1fr;
    column-gap: 5rem;
  }
`;
const Side = styled.aside`
  .precio {
    font-size: 2rem;
    color: #75AB00;
  }
  .cliente {
    margin-top: 4rem;
    border-radius: 2rem;
    background-color: #75AB00;
    padding: 3rem;
    color: #FFFFFF;

    p {
      margin: 0;
    }
  }
`;

/*
==================================================
          Query
==================================================
*/

export const data = graphql`
  query($id: String!) {
    allStrapiPropiedades( filter: { id: { eq: $id } } ) {
      nodes {
        nombre
        descripcion
        wc,
        precio,
        estacionamiento,
        habitaciones,
        categoria {
          id
          Nombre
        }
        agente {
          id
          Nombre
          Telefono
          Correo
        }
        imagen {
          sharp: childImageSharp {
            fluid ( quality: 90, maxWidth: 1200 ) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  }
`;

const Propiedad = ({ data: { allStrapiPropiedades: { nodes: [ propiedad ] } } }) => {

  const { agente, descripcion, estacionamiento, habitaciones, imagen: { sharp: { fluid } }, nombre, precio, wc } = propiedad;

  return (
    <Layout>
      <h1>{ nombre }</h1>
      <Contenido>
        <main>
          <Image
            fluid={ fluid }
            alt={ nombre }
          />
          <p>{ descripcion }</p>
        </main>
        <Side>
          <p className="precio">$ {precio}</p>
          <Iconos
            estacionamiento={ estacionamiento }
            habitaciones={ habitaciones }
            wc={ wc }
          />

          <div className="cliente">
            <h2>Vendedor</h2>
            <p>{ agente.Nombre }</p>
            <p>Tel: { agente.Telefono }</p>
            <p>Email: { agente.Correo }</p>
          </div>
        </Side>
      </Contenido>
    </Layout>
  );
};

export default Propiedad;