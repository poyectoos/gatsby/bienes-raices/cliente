import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
import { graphql } from 'gatsby';

/*
==================================================
          Components
==================================================
*/
import Layout from './Layout';

import Propiedades from './Propiedades';
/*
==================================================
          Emotion
==================================================
*/
const Contenido = styled.main`
  max-width: 1000px;
  margin: 0 auto;
  width: 95%;

  @media(min-width: 768px) {
    display: grid;
    grid-template-columns: 2fr 1fr;
    column-gap: 5rem;
  }
`;

/*
==================================================
          Query
==================================================
*/
export const data = graphql`
  query($id: String!) {
    allStrapiPaginas(filter: { id: { eq: $id } }) {
      nodes {
        id
        nombre: Nombre
        contenido: Contenido
        imagen: Imagen {
          sharp: childImageSharp {
            fluid ( quality: 90, maxWidth: 1200 ) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    }
  }
`;


const Paginas = ({ data: { allStrapiPaginas: { nodes: [pagina] } } }) => {

  const { nombre, contenido, imagen: { sharp: { fluid } } } = pagina;

  console.log(pagina);

  return (
    <Layout>
      <h1>{ nombre }</h1>
      <Contenido>
        <Image
          fluid={ fluid }
          alt={ nombre }
        />
        <p>{ contenido }</p>
      </Contenido>

      {
        nombre === 'Propiedades'
          &&
        <Propiedades />
      }
    </Layout>
  );
};

export default Paginas;