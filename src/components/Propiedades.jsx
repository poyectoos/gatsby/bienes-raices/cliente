import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';

/*
==================================================
          Components
==================================================
*/
import Propiedad from '../components/PropiedadCard';

import usePropiedades from '../hooks/usePropiedades';
import useFiltro from '../hooks/useFiltro';

import Style from '../css/propiedades.module.css';
/*
==================================================
          Emotion
==================================================
*/
const Subtitle = styled.h2`
  margin-top: 4rem;
`;

const Propiedades = () => {

  const { categoria, Select } = useFiltro();

  const data = usePropiedades();

  const [ propiedades ] = useState(data);
  const [ filtradas, setFiltradas ] = useState([]);

  useEffect(() => {
    if (categoria) {
      const filtro = propiedades.filter(propiedad => propiedad.categoria.Nombre === categoria);
      setFiltradas(filtro);
    } else {
      setFiltradas([...propiedades]);
    }
  }, [categoria, propiedades]);

  console.log(propiedades);

  return (
    <>
      <Subtitle>Nuestras propiedades</Subtitle>
      <Select />
      <ul className={ Style.propiedades }>
        {
          filtradas.map(
            propiedad => (
              <Propiedad
                key={ propiedad.id }
                propiedad={ propiedad }
              />
            )
          )
        }
      </ul>
    </>
  );
};

export default Propiedades;