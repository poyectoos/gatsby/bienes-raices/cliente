import React from 'react';
import { Link } from 'gatsby';
import styled from '@emotion/styled';
import { graphql, useStaticQuery } from 'gatsby';
/*
==================================================
          Componentes
==================================================
*/
import Navegation from './Navegation';
/*
==================================================
          Emotion
==================================================
*/
const Headerr = styled.header`
  background-color: #0D283B;
  padding: 0 1rem;
`;

const Container = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  text-align: center;

  @media (min-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }
`;
const Brand = styled(Link)`
  margin-top: 1rem;
`;

const Header = () => {

  const { logo } = useStaticQuery(
    graphql`
      query {
        logo: file(relativePath: { eq: "logo.svg" }) {
          publicURL
        }
      }
    `
  );
  
  return (
    <Headerr>
      <Container>
        <Brand to={'/'}>
          <img src={logo.publicURL} alt="Bienes Raices" />
        </Brand>
        <Navegation />
      </Container>
    </Headerr>
  );
};

export default Header;