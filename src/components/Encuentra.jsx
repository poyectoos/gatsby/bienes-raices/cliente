import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import BackgroundImage from 'gatsby-background-image';
import styled from '@emotion/styled';

/*
==================================================
          Components
==================================================
*/
import Hero from '../css/hero.module.css';
/*
==================================================
          Emotion
==================================================
*/
const Image = styled(BackgroundImage)`
  height: 300px;
`;

const Encuentra = () => {

  const { image: { sharp: { fluid } } } = useStaticQuery(
    graphql`
      query {
        image: file (relativePath: {eq: "encuentra.jpg" }) {
          sharp: childImageSharp {
            fluid( maxWidth: 1200  duotone: {
              highlight: "#222222", shadow: "#192550", opacity: 30
              }) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `
  );

  console.log();

  return (
    <Image
      tag="section"
      fluid={ fluid }
      fadeIn="soft"
    >
      <div className={ Hero.imagen }>
        <h3 className={ Hero.titulo } >Encuentra el lugar de tus sueños</h3>
        <p>15 años de experiencia</p>
      </div>
    </Image>
  );
};

export default Encuentra;