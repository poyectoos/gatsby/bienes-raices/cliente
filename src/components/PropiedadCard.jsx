import React from 'react';
import styled from '@emotion/styled';
import Image from 'gatsby-image';
import { Link } from 'gatsby';
import urlSlug from 'url-slug';

/*
==================================================
          Components
==================================================
*/
import Iconos from '../components/Iconos';

/*
==================================================
          Emotion
==================================================
*/
const Card = styled.div`
  border: 1px solid #e1e1e1;
  img {
    max-width: 100%;
  }
`;
const Contenido = styled.div`
  padding: 2rem;
`;
const Title = styled.h3`
  font-family: 'Lato', sans-serif;
  margin: 0 0 2rem 0;
  font-size: 2.4rem;
`;
const Description = styled.p`
  font-size: 2rem;
  color: #75AB00;
`;
const Button = styled(Link)`
  margin-top: 2rem;
  padding: 1rem;
  background-color: #75ab00;
  width: 100%;
  color: #fff;
  display: block;
  text-decoration: none;
  text-align: center;
  font-weight: 700;
  text-transform: uppercase;

  &:hover{
    cursor: pointer;
    background-color: #639100
  }
`;

const PropiedadCard = ({ propiedad }) => {

  const { estacionamiento, habitaciones, imagen, nombre, precio, wc } = propiedad;

  return (
    <Card>
      <Image
        fluid={ imagen }
      />
      <Contenido>
        <Title>{ nombre }</Title>

        <Description>$ { precio }</Description>
        <Iconos
          estacionamiento={ estacionamiento }
          habitaciones={ habitaciones }
          wc={ wc }
        />

        <Button
          to={ urlSlug(nombre) }
        >
          Ver propiedad
        </Button>
      </Contenido>
    </Card>
  );
};

export default PropiedadCard;